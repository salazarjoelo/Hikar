package freemap.hikar.ui

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.Menu
import android.widget.SearchView
import androidx.core.content.ContextCompat
import freemap.hikar.R
import kotlinx.android.synthetic.main.activity_location_picker.*
import kotlinx.coroutines.*
import org.json.JSONArray
import org.osmdroid.config.Configuration
import org.osmdroid.events.MapEventsReceiver
import org.osmdroid.tileprovider.tilesource.XYTileSource
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.CopyrightOverlay
import org.osmdroid.views.overlay.ItemizedIconOverlay
import org.osmdroid.views.overlay.MapEventsOverlay
import org.osmdroid.views.overlay.OverlayItem
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay
import java.net.URL

class LocationPickerActivity : ProgressBarActivity() {

    override val progressBarId = R.id.progressBarLocationPicker

    private var selectedLat = 91.0
    private var selectedLon = 181.0


    private var curLocationMarker: OverlayItem? = null
    private lateinit var markerOverlay: ItemizedIconOverlay<OverlayItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Configuration.getInstance().load(applicationContext, PreferenceManager.getDefaultSharedPreferences(this))

        setContentView(R.layout.activity_location_picker)


        val tileSource = XYTileSource(
            "OpenTopoMap",
            1,
            20,
            256,
            ".png",
            arrayOf(
                "https://a.tile.opentopomap.org/",
                "https://b.tile.opentopomap.org/",
                "https://c.tile.opentopomap.org/"
            ),
            "Map data \u00a9 OpenStreetMap contributors, ODBL; contours SRTM | Map display \u00a9 OpenTopoMap (CC-by-SA)"
        )


        mapView.setTileSource(tileSource)
        mapView.setMultiTouchControls(true)
        mapView.controller.setZoom(16.0)


        val startLat = intent?.getDoubleExtra("lat", 51.0503) ?: 51.0503
        val startLon = intent?.getDoubleExtra("lon", -0.7263) ?: -0.7263
        markerOverlay =
            ItemizedIconOverlay(arrayListOf(), ContextCompat.getDrawable(this, R.drawable.marker), null, this)
        setSelectedLocation(startLat, startLon, false)
        //   setSelectedLocation(50.9290, -1.4085)
        mapView.overlays.add(markerOverlay)


        mapView.overlays.add(CopyrightOverlay(this))

        val myLocationOverlay = MyLocationNewOverlay(GpsMyLocationProvider(applicationContext), mapView)
        myLocationOverlay.enableMyLocation()
        mapView.overlays.add(myLocationOverlay)

        val mapEventsReceiver = object : MapEventsReceiver {
            override fun longPressHelper(p: GeoPoint): Boolean {
                return false
            }

            override fun singleTapConfirmedHelper(p: GeoPoint): Boolean {
                setSelectedLocation(p.latitude, p.longitude)
                return true
            }
        }

        mapView?.overlays?.add(MapEventsOverlay(mapEventsReceiver))



        btnSelectLocation.setOnClickListener {
            if (selectedLat < 90 && selectedLon < 180) {
                val intent = Intent()
                intent.putExtra("lat", selectedLat)
                intent.putExtra("lon", selectedLon)
                setResult(RESULT_OK, intent)
                finish()
            } else {
                AlertDialog.Builder(this).setMessage("Please select a location by clicking on the map or searching")
                    .setPositiveButton("OK", null).show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    fun onSearch(q: String) {
        launch {
            try {
                showProgressBar(true)
                val url = URL("https://hikar.org/nomproxy.php?q=$q")
                var resp = ""
                withContext(Dispatchers.Default) {
                    resp = url.readText()
                }
                val json = JSONArray(resp)
                showProgressBar(false)
                when (json.length()) {
                    0 -> {
                        AlertDialog.Builder(this@LocationPickerActivity).setMessage("No matching search results")
                            .setPositiveButton("OK", null).show()
                    }

                    1 -> {
                        if (json.length() == 1) {
                            val obj = json.getJSONObject(0)
                            mapView.controller.setCenter(GeoPoint(obj.getDouble("lat"), obj.getDouble("lon")))
                        }
                    }

                    else -> {
                        val intent = Intent(this@LocationPickerActivity, LocationSearchResultsActivity::class.java)
                        intent.putExtra("json", resp)
                        startActivityForResult(intent, 0)
                    }
                }
            } catch (e: Exception) {
                if (!job.isCancelled) {
                    showProgressBar(false)
                    AlertDialog.Builder(this@LocationPickerActivity)
                        .setMessage("An error occurred; this is probably due to no network connection. Details: $e.")
                        .setPositiveButton("OK", null).show()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_location_picker, menu)
        val item = menu?.findItem(R.id.locationPickerSearch)
        val searchViewMap = item?.actionView as SearchView
        searchViewMap.queryHint = resources.getString(R.string.clickMap)
        searchViewMap.maxWidth = Integer.MAX_VALUE
        searchViewMap.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(q: String?): Boolean {
                return false
            }

            override fun onQueryTextSubmit(q: String?): Boolean {
                onSearch(q ?: "")
                searchViewMap.clearFocus()
                return true
            }
        })

        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0 && resultCode == RESULT_OK) {
            val lat = data?.getDoubleExtra("lat", 91.0) ?: 91.0
            val lon = data?.getDoubleExtra("lon", 181.0) ?: 181.0
            if (lat < 90 && lon < 180) {
                setSelectedLocation(lat, lon)
            }
        }
    }

    private fun setSelectedLocation(lat: Double, lon: Double, placeMarker: Boolean = true) {
        selectedLat = lat
        selectedLon = lon

        mapView.controller.setCenter(GeoPoint(selectedLat, selectedLon))



        if(placeMarker) {
            tvSelectedLocation.text = String.format("Selected location: lat %9.4f, lon %9.4f", lat, lon)
            markerOverlay.removeAllItems()
            curLocationMarker =
                OverlayItem("Current location", "Current selected location", GeoPoint(selectedLat, selectedLon))
            markerOverlay.addItem(curLocationMarker)
        } else {
            tvSelectedLocation.text = resources.getString(R.string.selectLocationInstructions)
        }
    }
}