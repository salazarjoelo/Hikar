/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import freemap.hikar.R

class PoiListAdapter(
    private var names: ArrayList<String>,
    private var types: ArrayList<String>,
    val onClick: (Int) -> Unit
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    inner class CustomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvPoiName = itemView.findViewById(R.id.poiName) as TextView
        val tvPoiType = itemView.findViewById(R.id.poiType) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return CustomViewHolder(layoutInflater.inflate(R.layout.poi_view, parent, false))
    }

    override fun getItemCount(): Int {
        return names.size
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, position: Int) {
        val holder = h as CustomViewHolder
        holder.tvPoiName.text = names[position]
        holder.tvPoiType.text = types[position]
        holder.itemView.setOnClickListener {
            holder.itemView.isSelected = true
            onClick(position)
        }
    }

    fun updateData(names: List<String>, types: List<String>) {
        this.names.clear()
        this.types.clear()
        this.names.addAll(names)
        this.types.addAll(types)
        notifyDataSetChanged()
    }
}