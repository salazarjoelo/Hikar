/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.ui

import android.widget.TextView
import java.util.*

class Status(val view: TextView) {
    var height = 0.0f
        set(h) {
            field = h
            updateText()
        }
    private var hFov = 0.0f
    private var origHfov = 0.0f


    fun setHfov(fov: Float) {
        hFov = fov
        origHfov = fov
        updateText()
    }

    fun updateHfov(newFov: Float) {
        hFov += newFov
        updateText()
    }

    private fun updateText() {
        view.text = String.format(
            Locale.US,
            "Elevation %8.3fm Hfov %8.3f (adjustment %8.3f) %s",
            height,
            hFov,
            hFov - origHfov,
            if (hFov - origHfov > 0.1) "- ZOOMED OUT" else if (hFov - origHfov < -0.1) "- ZOOMED IN" else ""
        )
    }
}