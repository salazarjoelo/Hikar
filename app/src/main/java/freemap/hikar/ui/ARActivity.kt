/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.ui

import android.Manifest
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.hardware.GeomagneticField
import android.hardware.SensorManager
import android.location.Location
import android.opengl.Matrix
import android.os.Bundle
import android.view.ViewGroup.LayoutParams
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import freemap.data.GoogleProjection
import freemap.data.Point
import freemap.hikar.R
import freemap.hikar.camera.CameraInterface

import freemap.hikar.location.LocationProcessor
import freemap.hikar.sensor.SensorInput
import freemap.hikar.startWithTexCheck
import io.fotoapparat.Fotoapparat
import io.fotoapparat.selector.back
import kotlinx.android.synthetic.main.activity_main.*


abstract class ARActivity : CoroutineActivity() {

    private val projection = GoogleProjection()

    private val sensorInput = SensorInput(this::receiveSensorInput)
    private lateinit var locationProcessor: LocationProcessor
    private var lastLon = -0.7263
    private var lastLat = 51.0503

    private var field: GeomagneticField? = null
    private lateinit var hud: HUD
    private lateinit var status: Status

    private var enableOrientationAdjustment = true
    private var orientationAdjustment = 0.0f


    private val initHfov = 40.0f

    enum class PermissionState { NOT_GRANTED, GRANTED, DENIED }

    private var permissionState = PermissionState.NOT_GRANTED


    private var fotoapparat: Fotoapparat? = null
    private lateinit var cameraInterface: CameraInterface


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        hud = HUD(this)
        status = Status(statusView)

        openGLView.onCreate(
            projection
        ) {

            if (permissionState == PermissionState.GRANTED) {

                fotoapparat?.start()
            }

        }

        openGLView.renderer.setHFOV(initHfov)
        status.setHfov(initHfov)

        addContentView(hud, LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT))

        locationProcessor = LocationProcessor(this, 1000, 2.0f, ::receiveLocation)

        sensorInput.attach(this)

        openGLView.setOnTouchListener(PinchListener {
            openGLView.renderer.changeHFOV((5 * it).toFloat())
            status.updateHfov((5 * it).toFloat())
            hud.invalidate()

        })

        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        fotoapparat = Fotoapparat(context = this, view = openGLView.renderer, lensPosition = back())

        cameraInterface = CameraInterface(this) {
            openGLView.renderer.setHFOV(it)
            status.setHfov(it)
        }
    }

    override fun onResume() {
        super.onResume()
        val permissions = arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        if (permissions.all {
                ContextCompat.checkSelfPermission(
                    this,
                    it
                ) == PackageManager.PERMISSION_GRANTED
            }) {

            permissionState = PermissionState.GRANTED
            fotoapparat?.startWithTexCheck(openGLView)
            cameraInterface.getHfov()
            //  gl.onResume()

            sensorInput.start()

            locationProcessor.startUpdates()

        } else if (permissionState != PermissionState.DENIED) {

            ActivityCompat.requestPermissions(
                this, permissions, 0
            )
        }
    }

    override fun onPause() {
        super.onPause()
        locationProcessor.stopUpdates()
        sensorInput.stop()
        fotoapparat?.stop()
    }

    override fun onDestroy() {
        sensorInput.detach()
        super.onDestroy()
    }

    private fun receiveLocation(loc: Location) {
       setLocation(loc.longitude, loc.latitude, true)
    }


    private fun setLocation(lon: Double, lat: Double, gpsLocation: Boolean) {

        val p = Point(lon, lat)

        if (field == null) {
            field = GeomagneticField(lat.toFloat(), lon.toFloat(), p.z.toFloat(), System.currentTimeMillis())
        }


        if (gpsLocation) {
            lastLon = lon
            lastLat = lat
        }

        dataUpdate(p, gpsLocation)
    }

    abstract fun dataUpdate(p: Point, gpsLocation: Boolean)

    private fun receiveSensorInput(glR: FloatArray, bearingRadians: Float) {
        val orientation = FloatArray(3)
        val magNorth = field?.declination ?: 0.0f
        val actualAdjustment = magNorth + (if (enableOrientationAdjustment) orientationAdjustment else 0.0f)
        Matrix.rotateM(glR, 0, actualAdjustment, 0.0f, 1.0f, 0.0f)
        SensorManager.getOrientation(glR, orientation)
        openGLView.renderer.setOrientMtx(glR)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            0 -> {
                if (grantResults.isNotEmpty() && (grantResults[0] == PackageManager.PERMISSION_DENIED || grantResults[1] == PackageManager.PERMISSION_DENIED || grantResults[2] == PackageManager.PERMISSION_DENIED)) {
                    permissionState = PermissionState.DENIED
                    AlertDialog.Builder(this)
                        .setMessage("Hikar will not work without camera, GPS and file permissions and will therefore now finish.")
                        .setPositiveButton("OK") { _, _ -> finish() }.show()
                } else {
                    permissionState = PermissionState.GRANTED
                    fotoapparat?.startWithTexCheck(openGLView)
                }
            }
        }
    }
}
