/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.signposting

import freemap.data.Algorithms
import freemap.data.Point
import java.util.*


class Path {
    val vertices = arrayListOf<Vertex>()
    val edges = arrayListOf<Graph.Edge>()
    val numJunctions: Int
        get() {
            return if (vertices.size <= 2) 0 else vertices.subList(1, vertices.size-1).filter { it.edges.size >= 3 }.size
        }
    fun isEmpty() : Boolean{
        return vertices.isEmpty() || edges.isEmpty()
    }

    // get the number of junctions in a path
    // A junction vertex must have at least three edges
    // This aims to help noticeboard generation: we place a noticeboard if a route lacks junction vertices.
    // Non-junction vertices might be e.g. a change from a track to a footway. If there's an issue along the
    // footway, it won't be shown if we use a one-edge route as the criterion to place a noticeboard, but if we
    // test for zero junctions then it will.
    /*
    fun getNumJunctions(): Int {
        return vertices.subList(1, vertices.size).filter { it.edges.size >= 3 }.size
    }
    */
}

// optimised for Dijkstra, i.e. has a distance and a parent
// less reusable in other contexts but saves having to create additional objects later
class Vertex(val id: Int, val p: Point, val poiVertex: Boolean = false): Comparable<Vertex> {
    var distance = Double.POSITIVE_INFINITY
    var weightedDistance = Double.POSITIVE_INFINITY
    val edges = arrayListOf<Graph.Edge>()
    var parent: Graph.Edge? = null
    var visited = false
    fun addEdge(e: EdgeData, other: Vertex) {
        edges.add(Graph.Edge(e, this, other))
    }

    override fun equals (other: Any?) : Boolean {
        return p == (other as Vertex).p
    }
    override fun compareTo(other: Vertex): Int {
        return distance.compareTo(other.distance)
    }
}

//data class EdgeData(val wayId: Long, val localId: Int, val localIdWay: GraphManager.LocalIdWay, val dist: Double,  val fromIdx: Int, val toIdx: Int, val isWalkingRoute: Boolean)
class EdgeData(val localIdWay: GraphManager.LocalIdWay, val localId: Int, val dist: Double, val fromIdx: Int, val toIdx: Int, val isWalkingRoute: Boolean) {
    fun reverse(): EdgeData {
        return EdgeData(localIdWay, localId, dist, toIdx, fromIdx, isWalkingRoute)
    }
}

class Graph {

    class Edge(val data: EdgeData, val source: Vertex, val target: Vertex) {

        private var initBearing = Double.MAX_VALUE

        private fun getOrderedIndicesFrom(node: Point): Pair<Int, Int> {
            return if (node == source.p) Pair(data.fromIdx, data.toIdx) else Pair(data.toIdx, data.fromIdx)
        }

        fun progressionFrom(node: Point): IntProgression {
            val indices = getOrderedIndicesFrom(node)
            return doGetProgression(indices.first, indices.second)
        }

        private fun doGetProgression(index1: Int, index2: Int) : IntProgression {
            return IntProgression.fromClosedRange(
                index1,
                index2,
                if (index1 > index2) -1 else 1
            )
        }

        private fun progression(): IntProgression {
            return IntProgression.fromClosedRange(
                data.fromIdx,
                data.toIdx,
                if(data.fromIdx > data.toIdx) -1 else 1

            )
        }

        fun getIndex(node: Point): Int {
            return if (node == source.p) data.fromIdx else data.toIdx
        }

        fun getPoints(): ArrayList<Point> {
            val points = arrayListOf<Point>()
            progression().forEach { idx ->
                points.add(data.localIdWay.way.getPoint(idx))
            }
            return points
        }

        override fun equals(other: Any?): Boolean {
            val otherEdge = other as Edge
            return data.localId == otherEdge.data.localId && data.fromIdx == otherEdge.data.fromIdx && data.toIdx == otherEdge.data.toIdx
        }

        /// **** 160719 ****
        fun isReverseOf(other: Any?): Boolean {
            val otherEdge = other as Edge
            return data.localId == otherEdge.data.localId && data.fromIdx == otherEdge.data.toIdx && data.toIdx == otherEdge.data.fromIdx
        }

        fun getInitialBearing(): Double {
            if (initBearing > 360.0) {
                // We need the renderedRoute's initial bearing to draw the arms correctly, this should be the bearing from
                // the initial node to the next node further than some threshold distance

                /*
                Log.d(
                    "hikar",
                    "getInitialBearing(): from ${data.fromIdx} to ${data.toIdx} osmid ${data.localIdWay.way.id}"
                )
                */
                var dist = 0.0
                val countedNodeDistThreshold = 2.0
                val iter = progression().iterator()

                var i = iter.nextInt()
            //    Log.d("hikar", "Iterator (initial): $i")
                val firstPoint = data.localIdWay.way.getPoint(i)
                var countedPoint = Point()

                while (dist < countedNodeDistThreshold && iter.hasNext()) {
                    i = iter.nextInt()
             //       Log.d("hikar", "Iterator : $i")
                    countedPoint = data.localIdWay.way.getPoint(i)
                    dist = Algorithms.haversineDist(firstPoint.x, firstPoint.y, countedPoint.x, countedPoint.y)
                }

                initBearing = countedPoint.bearingFrom(firstPoint)
            }
            return initBearing
        }
    }

    val vertices = arrayListOf<Vertex>()


    fun addVertex(p: Point, poiVertex: Boolean = false): Int {
        val id = vertices.size
        vertices.add(Vertex(id, p, poiVertex))
        return id
    }

    fun addEdge(vtxIdx1: Int, vtxIdx2: Int, e: EdgeData) {
        val n1 = vertices[vtxIdx1]
        val n2 = vertices[vtxIdx2]

        n1.addEdge(e, n2)
        n2.addEdge(e.reverse(), n1)
    }

    fun dijkstra(p1: Point, p2: Point) : Path {
        return dijkstra(findNearestVertex(p1), findNearestVertex(p2))
    }

    private fun dijkstra(vtx1: Vertex?, vtx2: Vertex?) : Path {
        val path = Path()
        if(vtx1 != null && vtx2 != null) {
            val openList = PriorityQueue<Vertex>()
            var curDistance: Double
            var curVertex: Vertex = vtx1
            var weighting : Double

            vertices.forEach {
                it.distance = Double.POSITIVE_INFINITY
                it.visited = false
                it.parent = null
            }

            curVertex.distance = 0.0
            curVertex.weightedDistance = 0.0
            openList.add(curVertex)

            while (curVertex != vtx2 && openList.isNotEmpty()) {

                curVertex = openList.poll()
                curVertex.visited = true

                curVertex.edges.forEach {
                    weighting = if (it.data.isWalkingRoute) 1.0 else 1.25
          //          weighting = 1.0

                    if (!it.target.visited) {

                        curDistance = curVertex.distance + it.data.dist*weighting
                        if (curDistance < it.target.distance) {

                            openList.remove(it.target)

                            it.target.distance = curDistance
                            it.target.parent = it


                            openList.add(it.target)

                        } /*else if (!openList.contains(it.target)) {
                            openList.add(it.target)
                        }*/
                    }
                }
            }

            if (curVertex == vtx2) {
                path.vertices.add(vtx2)
                var edgeFromVertex = vtx2.parent
                while (edgeFromVertex != null) {
                    path.vertices.add(0, edgeFromVertex.source)
                    path.edges.add(0, edgeFromVertex)
                    edgeFromVertex = edgeFromVertex.source.parent
                }
            }
        }
        return path
    }

    fun clear() {
        vertices.clear()
    }

    fun findNearestVertex(p: Point): Vertex? {
        var dist : Double
        var lowestDist = Double.MAX_VALUE
        var vertex: Vertex? = null
        vertices.forEach {
            dist = Algorithms.haversineDist(it.p.x, it.p.y, p.x, p.y)
            if (dist < lowestDist) {
                lowestDist = dist
                vertex = it
            }
        }
        return vertex
    }
}