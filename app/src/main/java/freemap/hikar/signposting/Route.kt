/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.signposting

import freemap.data.Algorithms
import freemap.data.Point

class Route(var numJunctions: Int) {

    val segments = mutableListOf<Segment>()
    var initialBearing = 0.0
    var distance = 0.0

    data class Segment(
        val p1: Point,
        val p2: Point,
        val wayId: Long,
        val distance: Double,
        val isWalkingRoute: Boolean,
        val unprojectedPoints: List<Point>
    )

    fun addSegment(
        p1: Point,
        p2: Point,
        wayId: Long,
        dist: Double,
        isWalkingRoute: Boolean,
        unprojectedPoints: List<Point>,
        atStart: Boolean = false
    ) {
        if(unprojectedPoints.size >= 2) {
            segments.add(
                if (atStart) 0 else segments.size,
                Segment(p1, p2, wayId, dist, isWalkingRoute, unprojectedPoints)
            )
            distance += dist
        }
    }

    // is this mostly on walking routes?
    // idea being we don't signpost to destinations where more than 50% of the renderedRoute is on roads
    // However, all routes of less than 1.5km are accepted

    fun isMostlyWalkingRoute(): Boolean {
        var wrDist = 0.0
        segments.forEach {
            if (it.isWalkingRoute) {
                wrDist += it.distance
            }
        }
        return distance < 15000 || wrDist / distance >= 0.5
    }

    fun getPointOnPath(distance: Double): Point? {
        if(distance <= this.distance) {
            var distSoFar = 0.0
            var curDist = 0.0
            var segIdx = -1
            var ptIdx = 1
            while (distSoFar < distance && segIdx+1 < segments.size) {
                segIdx++
                ptIdx = 0
                while (distSoFar < distance && ptIdx+1 < segments[segIdx].unprojectedPoints.size) {
                    ptIdx++
                    curDist = Algorithms.haversineDist(
                        segments[segIdx].unprojectedPoints[ptIdx - 1].x,
                        segments[segIdx].unprojectedPoints[ptIdx - 1].y,
                        segments[segIdx].unprojectedPoints[ptIdx].x,
                        segments[segIdx].unprojectedPoints[ptIdx].y
                    )
                    distSoFar += curDist

                }
            }

            if (segIdx < segments.size && ptIdx < segments[segIdx].unprojectedPoints.size) {
                val prop = 1 - ((distSoFar - distance) / curDist)
                return Point(
                    segments[segIdx].unprojectedPoints[ptIdx - 1].x + prop * (segments[segIdx].unprojectedPoints[ptIdx].x - segments[segIdx].unprojectedPoints[ptIdx - 1].x),
                    segments[segIdx].unprojectedPoints[ptIdx - 1].y + prop * (segments[segIdx].unprojectedPoints[ptIdx].y - segments[segIdx].unprojectedPoints[ptIdx - 1].y)
                )
            }
        }
        return null
    }

    fun asPoints(): List<Point> {
        val points = arrayListOf<Point>()

        if(segments.isNotEmpty()) {
            points.addAll(segments[0].unprojectedPoints)
            segments.subList(1, segments.size).forEach {
                points.addAll(it.unprojectedPoints.subList(1, it.unprojectedPoints.size))
            }
        }
        return points
    }
}