package freemap.hikar.signposting

/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */


import freemap.data.Point
import kotlin.math.abs

class Signpost(override val position: Point) : Sign {
    val arms = arrayListOf<Arm>()
    val id = 0L

    fun addArm(arm: Arm) {
        arms.add(arm)
    }

    fun getArmWithBearing(bearing: Double, threshold: Double): Arm? {
        var lowestSoFar = 360.0
        var closestArm: Arm? = null
        arms.forEach {
            val current = abs(it.bearing - bearing) % 360
            if (current < lowestSoFar && current <= threshold) {
                lowestSoFar = current
                closestArm = it
            }
        }
        return closestArm
    }


    // avoid "Track" or "Service" signposts which generally wouldn't happen in real life
    fun pruneArms() {
        var i = 0
        while (i < arms.size) {
            if (!arms[i].hasDestinations()) {
                if (arrayOf("Track", "Service", "Steps").contains(arms[i].getLabel())) {
                    arms.removeAt(i--)
                }
            }
            i++
        }
    }

    // 0.2.5 added
    fun finaliseArms() {
        arms.forEach { it.finaliseDestinations() }
    }

    fun hasArms(): Boolean {
        return arms.isNotEmpty()
    }

    override fun getSignPlates(): Array<SignPlate> {
        return arms.toTypedArray()
    }


    override fun toString(): String {
        return arms.joinToString { "\nMEXT ARM:" }
    }

    override fun getVerticalDisplacement (plateIdx: Int): Float {
        return 0f
    }
}