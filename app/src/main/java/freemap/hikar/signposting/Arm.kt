package freemap.hikar.signposting

import freemap.hikar.opengl.ModelTextOverlay

/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */



class Arm(val bearing: Double, private val tags: HashMap<String, String>) : SignPlate {

    private var destinations = arrayListOf<Destination>()


    fun addDestination(d: Destination) {
        destinations.add(d)
        destinations.sortBy { it.getWeightedDistance() }
    }


    private fun getDestinationTexts(n: Int): Array<String> {
        val n2 = if (n > destinations.size) destinations.size else n
        return if (n2 > 0) {
            destinations.map { it.toString() }.toTypedArray()
        } else {
            arrayOf(getLabel())
        }
    }

    fun hasDestinations(): Boolean {
        return destinations.size > 0
    }

    fun getLabel(): String {
        val tagValue =
            (if (tags.containsKey("designation")) tags["designation"] else if (tags.containsKey("highway")) tags["highway"] else "")
                ?: ""
        return if (tagValue.length >= 2) tagValue.substring(0, 1).toUpperCase() + tagValue.substring(1).replace(
            "_",
            " "
        ) else tagValue
    }

    // 0.2.5 sort destinations by distance, limit to 2, and ensure that destination 2 doesn't have a lower priority than destination 1
    fun finaliseDestinations() {

        val newDestinations = arrayListOf<Destination>()
        if (destinations.size > 0) {
            newDestinations.add(destinations[0])
            if (destinations.size > 1 && (destinations[1].getWeighting() <= destinations[0].getWeighting() || destinations[1].distance <= destinations[0].distance)) {
                newDestinations.add(destinations[1])
            }
        }
        newDestinations.sortBy { it.distance }
        destinations = newDestinations
    }

    override fun getAngle(): Double {
        return bearing
    }

    override fun getText(): Array<String> {
        return if (hasDestinations()) getDestinationTexts(2) else arrayOf(getLabel())
    }

    override fun toString(): String {
        var s = ""
        destinations.forEach {
            s += "DESTINATION: $it\n"
        }
        return s
    }

    override fun getModelTextOverlay(): ModelTextOverlay {
        return ModelTextOverlay(1.9386f, 29.4f, 60.9538f, 16.6f, -0.3519f, 0.3518f, 0.5f, 2)
    }

    override fun getSignAsset(): String {
        return "nick_metal_sign_post_arm.obj"
    }
}
