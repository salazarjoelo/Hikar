/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.opengl

import android.content.Context
import android.graphics.SurfaceTexture
import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.opengl.Matrix
import android.util.AttributeSet
import freemap.data.Point
import freemap.data.Projection
import freemap.data.Way
import freemap.datasource.FreemapDataset
import freemap.datasource.OSMTiles
import freemap.datasource.TiledData
import freemap.hikar.signposting.*
import freemap.jdem.DEM
import io.fotoapparat.exception.camera.UnavailableSurfaceException
import io.fotoapparat.parameter.Resolution
import io.fotoapparat.parameter.ScaleType
import io.fotoapparat.view.CameraRenderer
import io.fotoapparat.view.Preview

import java.io.IOException
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10


fun SurfaceTexture.toPreview(): Preview.Texture {
    return Preview.Texture(this)
}

class OpenGLView(
    ctx: Context,
    attr: AttributeSet
) : GLSurfaceView(ctx, attr) {

    val renderer = DataRenderer()
    lateinit var proj: Projection
    lateinit var onTextureAvailable: (SurfaceTexture) -> Unit

    init {
        //   Log.d("hikar", "Initialising OpenGLView")
        setEGLContextClientVersion(2)
        setRenderer(renderer)
        renderMode = RENDERMODE_WHEN_DIRTY
    }


    fun onCreate(proj: Projection, onTextureAvailable: (SurfaceTexture) -> Unit) {
        this.proj = proj
        this.onTextureAvailable = onTextureAvailable
    }


    inner class DataRenderer : Renderer, FreemapDataset.WayVisitor, CameraRenderer {


        private lateinit var textureInterface: TextureGPUInterface
        private var cameraFeed: SurfaceTexture? = null
        private val nearPlane = 0.5f
        private val farPlane = 5000.0f
        private val perspectiveMtx = FloatArray(16)
        private var hFov = 40.0f
        private var viewMtx = FloatArray(16)
        private var lastOrientMtx = FloatArray(16)
        private var loadingData = false

        // 180215 replace HashMap of renderedWays with array list.
        // There is now no need to index the rendered ways by wayId (idea was duplicate prevention), as we are
        // using tiled FreemapDatasets in a HashMap indexed by bottom left coords and ways are split at tile boundaries.
        // Furthermore having one HashMap spanning all FreemapDatasets (as was) will lead to the problem
        // in which we have 2 ways (one split localIdWay) with the same ID across different tiles meaning that if
        // we use a HashMap with the ID as the index, only one will be added

        private val renderedWays = arrayListOf<RenderedWay>()
        private val renderedDems = hashMapOf<String, RenderedDEM>()
        private val receivedDatasets = hashMapOf<String, FreemapDataset>()
        private val signModels = arrayListOf<CompoundModel>()
        private var preventRender = false
        private var renderedRoute: RenderedWay? = null

        private val cameraRect = GLRect(
            floatArrayOf(
                -1.0f, 1.0f, 0.0f,
                -1.0f, -1.0f, 0.0f,
                1.0f, -1.0f, 0.0f,
                1.0f, 1.0f, 0.0f
            ), null
        )

        private var zDisp = 1.4f
        private var cameraPos = Point()
        private var manualDisp = Point()
        private lateinit var gpuInterface: GPUInterface
        private lateinit var signpostTextureInterface: TextureGPUInterface
        private lateinit var signpostTextTextureInterface: TextureGPUInterface
        private lateinit var modelMaker: SignpostModelMaker

        private val textureMaker = TextureMaker(context)

        init {

            Matrix.setIdentityM(viewMtx, 0)
            Matrix.setIdentityM(perspectiveMtx, 0)
        }

        override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {

            GLES20.glClearColor(0.0f, 0.0f, 0.3f, 0.0f)
            GLES20.glClearDepthf(1.0f)
            GLES20.glEnable(GLES20.GL_BLEND)
            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA)
            GLES20.glEnable(GLES20.GL_DEPTH_TEST)
            GLES20.glDepthFunc(GLES20.GL_LEQUAL)

            //       Log.d("hikar", "onSurfaceCreated()")

            val vertexShader = "attribute vec4 aVertex;\n" +
                    "uniform mat4 uPerspMtx, uViewMtx;\n" +
                    "void main(void)\n" +
                    "{\n" +
                    "gl_Position = uPerspMtx * uViewMtx * aVertex;\n" +
                    "}\n"
            val fragmentShader = "precision mediump float;\n" +
                    "uniform vec4 uColour;\n" +
                    "void main(void)\n" +
                    "{\n" +
                    "gl_FragColor = uColour;\n" +
                    "}\n"

            gpuInterface = GPUInterface("normal", vertexShader, fragmentShader)


            try {
                val signpostTexVertexShader = "attribute vec4 aVertex;\n" +
                        "attribute vec2 aTexCoord;\n" +
                        "varying vec2 vTextureValue;\n" +
                        "uniform mat4 uPerspMtx, uViewMtx, uArmMtx, uModelMtx;\n" +
                        "void main(void)\n" +
                        "{\n" +
                        "gl_Position = uPerspMtx * uViewMtx * uModelMtx * uArmMtx * aVertex;\n" +
                        // Note does t coordinate need to be inverted? Check - YES - but maybe no for wood?
                        //  "vTextureValue - aTexCoord;\n" +
                        "vTextureValue = vec2(aTexCoord.s, 1.0 - aTexCoord.t);\n" +
                        "}\n"
                val signpostTexFragmentShader = "precision mediump float;\n" +
                        "varying vec2 vTextureValue;\n" +
                        "uniform sampler2D uTexture;\n" +
                        "uniform vec4 uColour;\n" +
                        "void main(void)\n" +
                        "{\n" +
                        "gl_FragColor = texture2D(uTexture,vTextureValue);\n" +
                        "}\n"

                signpostTextureInterface = textureMaker.makeTextureGPUInterface(
                    "signpost_textures.png",
                    signpostTexVertexShader,
                    signpostTexFragmentShader,
                    GLES20.GL_TEXTURE1
                )

                signpostTextTextureInterface = textureMaker.makeTextureGPUInterface(
                    "european.png",
                    signpostTexVertexShader,
                    signpostTexFragmentShader,
                    GLES20.GL_TEXTURE2
                )

                modelMaker = SignpostModelMaker(
                    "nick_metal_sign_post.obj",
                    signpostTextTextureInterface
                )

            } catch (e: Exception) {
                //   Log.e("hikar", "Error loading signpost texture: $e")
            }

            // http://stackoverflow.com/questions/6414003/using-surfacetexture-in-android
            val glTextureExternalOes = 0x8d65
            val textureId = IntArray(1)
            GLES20.glGenTextures(1, textureId, 0)
            if (textureId[0] != 0) {

                val texVertexShader = "attribute vec4 aVertex;\n" +
                        "varying vec2 vTextureValue;\n" +
                        "void main (void)\n" +
                        "{\n" +
                        "gl_Position = aVertex;\n" +
                        "vTextureValue = vec2(0.5*(1.0 + aVertex.x), 0.5*(1.0-aVertex.y));\n" +
                        "}\n"
                val texFragmentShader = "#extension GL_OES_EGL_image_external: require\n" +
                        "precision mediump float;\n" +
                        "varying vec2 vTextureValue;\n" +
                        "uniform samplerExternalOES uTexture;\n" +
                        "void main(void)\n" +
                        "{\n" +
                        "gl_FragColor = texture2D(uTexture,vTextureValue);\n" +
                        "}\n"

                GLES20.glBindTexture(glTextureExternalOes, textureId[0])
                GLES20.glTexParameteri(glTextureExternalOes, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST)
                GLES20.glTexParameteri(glTextureExternalOes, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST)

                textureInterface = TextureGPUInterface(
                    "cameraTexture",
                    texVertexShader,
                    texFragmentShader,
                    textureId[0],
                    GLES20.GL_TEXTURE0
                ).apply {
                    setUniform1i("uTexture", 0)
                    selectTexture()
                }

                cameraFeed = SurfaceTexture(textureId[0])
                onTextureAvailable(cameraFeed!!)
                //latch.countDown()
            }
        }


        override fun onDrawFrame(gl: GL10?) {

            Matrix.setIdentityM(perspectiveMtx, 0)
            val aspectRatio = width.toFloat() / height.toFloat()
            Matrix.perspectiveM(
                perspectiveMtx, 0, hFov / aspectRatio, aspectRatio,
                nearPlane, farPlane
            )


            GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT or GLES20.GL_DEPTH_BUFFER_BIT)


            GLES20.glDisable(GLES20.GL_DEPTH_TEST)

            cameraFeed?.updateTexImage()

            // Not sure what the below was supposed to do !!!
            //val tm = FloatArray(16)
            //cameraFeed.getTransformMatrix(tm)

            textureInterface.select()
            cameraRect.draw(textureInterface)

            GLES20.glEnable(GLES20.GL_DEPTH_TEST)

            gpuInterface.select()
            if (!loadingData) {
                viewMtx = lastOrientMtx.clone()
                Matrix.translateM(
                    viewMtx, 0, -(cameraPos.x + manualDisp.x).toFloat(), -(cameraPos.y + manualDisp.y).toFloat(),
                    -(cameraPos.z + zDisp).toFloat()
                )

                gpuInterface.sendMatrix(viewMtx, "uViewMtx")
                gpuInterface.sendMatrix(perspectiveMtx, "uPerspMtx")
                if (renderedWays.size > 0) {
                    signpostTextureInterface.select()
                    signpostTextureInterface.sendMatrix(viewMtx, "uViewMtx")
                    signpostTextureInterface.sendMatrix(perspectiveMtx, "uPerspMtx")
                    signpostTextTextureInterface.select()
                    signpostTextTextureInterface.sendMatrix(viewMtx, "uViewMtx")
                    signpostTextTextureInterface.sendMatrix(perspectiveMtx, "uPerspMtx")
                    gpuInterface.select()


                    // 260918 Moved this from outside the empty renderedWays check above as surely we want to apply the matrices first???

                    // Prevent the ConcurrentModificationException, This is supposed to happen because you're
                    // adding to the renderedDEMs while iterating through them, and you can't add to a
                    // collection at the same time as iterating through it
                    // don't try to do this if loading data otherwise the synchronized block will prevent
                    // access to renderedDEMs when loading data


                    synchronized(renderedDems) {
                        renderedDems.forEach {
                            if (it.value.centreDistanceTo(cameraPos) <= farPlane) {
                                it.value.render(gpuInterface)
                            }
                        }
                    }



                    synchronized(renderedWays) {
                        renderedWays.forEach {
                            if (it.isDisplayed && it.distanceTo(cameraPos) <= farPlane) {
                                it.draw(gpuInterface)
                            }
                        }
                    }

                    renderedRoute?.draw(gpuInterface)

                    if (!preventRender) {
                        synchronized(signModels) {
                            signModels.forEach {
                                signpostTextureInterface.selectTexture()
                                it.draw(signpostTextureInterface)
                            }
                        }
                    }
                }
            }
        }

        override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {

            // need to get the camera parameters
            GLES20.glViewport(0, 0, width, height)
            val aspectRatio = width.toFloat() / height.toFloat()
            Matrix.setIdentityM(perspectiveMtx, 0)
            Matrix.perspectiveM(perspectiveMtx, 0, hFov / aspectRatio, aspectRatio, nearPlane, farPlane)
        }


        fun setCameraLocation(point: Point) {
            cameraPos = proj.project(point)
            requestRender()
        }

        fun reRenderSigns(signList: List<Sign>) {
            preventRender = true
            signModels.clear()
            renderSigns(signList)
        }


        fun renderSigns(signs: List<Sign>) {
            try {
                preventRender = true
                loadingData = true
                signs.forEach {
                    modelMaker.make(
                        context,
                        it,
                        proj.project(it.position)
                    ).apply {
                        scale(0.08f)
                        buffer()
                        signModels.add(this)
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
            } finally {
                preventRender = false
                loadingData = false
            }
        }

        fun setOrientMtx(glR: FloatArray) {
            lastOrientMtx = glR.clone()
            requestRender()
        }

        fun setRenderData(osm: OSMTiles, hgt: HashMap<String, TiledData?>) {
            loadingData = true
            setDemRenderData(hgt)
            setOsmRenderData(osm)
            loadingData = false
        }

        private fun setDemRenderData(hgt: HashMap<String, TiledData?>) {
            synchronized(renderedDems) {
                hgt.forEach {
                    if (renderedDems[it.key] == null && it.value != null) {
                        renderedDems[it.key] = RenderedDEM(it.value as DEM)
                    }
                }
            }
        }

        private fun setOsmRenderData(osm: OSMTiles) {
            osm.tilesAsMap().forEach {
                if (receivedDatasets[it.key] == null && it.value != null) {
                    val curOSM = it.value as FreemapDataset
                    receivedDatasets[it.key] = curOSM
                    curOSM.operateOnWays(this@DataRenderer)

                }
            }
        }

        fun setHFOV(hFov: Float) {
            this.hFov = hFov
        }

        fun changeHFOV(amount: Float) {
            setHFOV(this.hFov + amount)
        }


        override fun visit(w: Way) {
            synchronized(renderedWays) {
                if (w.isWalkingRoute() || w.isWalkableRoad()) {
                    renderedWays.add(RenderedWay(w, 2.0f))
                }
            }
        }

        override fun setScaleType(scaleType: ScaleType) {
            // Not implemented
        }


        override fun setPreviewResolution(resolution: Resolution) {
            // Not implemented
        }


        // looked at the Fotoapparat source code to figure out (hopefully) what to do here.
        // Fotoapparat requires an object which implements CameraRenderer to show the camera feed.
        // The getPreview() method of CameraRenderer returns a Preview created from a SurfaceTexture.
        // It uses the internal toPreview() extension function of SurfaceTexture which in turn creates a
        // Texture object from the SurfaceTexture, so we do this.
        // Also use the latch approach used by Fotoapparat to ensure we wait for the surface texture if it's not available yet.
        override fun getPreview(): Preview {
            return cameraFeed?.toPreview() ?: throw UnavailableSurfaceException()
        }


        fun isTextureAvailable(): Boolean {
            return cameraFeed != null
        }

        fun setRoute(routeUnprojected: List<Point>) {
            loadingData = true
            val route = routeUnprojected.map {
                val projected = proj.project(it)
                Point(projected.x, projected.y, projected.z + 0.1)
            }
            renderedRoute = RenderedWay(route = route, width = 5.0f)
            loadingData = false
        }

        fun routeOff() {
            renderedRoute = null
        }

        fun clearOsmData() {
            loadingData = true
            renderedWays.clear()
            renderedDems.clear()
            receivedDatasets.clear()
            loadingData = false
        }

    }
}
