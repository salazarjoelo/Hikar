package freemap.hikar.datasource


import freemap.data.*
import freemap.data.Annotation
import org.json.JSONObject
import org.json.JSONArray

import java.io.InputStream

import freemap.datasource.FreemapDataset
import freemap.datasource.RawDataSource
import java.util.ArrayList

/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

object GeoJSONDataInterpreter {


    fun getData(inp: InputStream): FreemapDataset {

        val json = RawDataSource.doLoad(inp)
        return getData(json)
    }

    fun getData(json: String): FreemapDataset {

        val dataset = FreemapDataset()
        dataset.projection = GoogleProjection()

        val jsonObj = JSONObject(json)


        val features = jsonObj.getJSONArray("features")
        for (i in 0 until features.length()) {
            val feature = features.getJSONObject(i)
            val geometry = feature.getJSONObject("geometry")
            val coordinates = geometry.getJSONArray("coordinates")

            when (val type = geometry.getString("type")) {
                "Point" -> {
                    val props = feature.getJSONObject("properties")
                    if (props.has("annotation") && props.getString("annotation") == "yes") {
                        val curFeature = makeAnnotation(coordinates, props)
                        dataset.add(curFeature)
                    } else {
                        val curFeature = makePOI(coordinates)
                        addProperties(curFeature, props)
                        dataset.add(curFeature)
                    }
                }
                "LineString", "MultiLineString", "Polygon", "MultiPolygon" -> {
                    val curWays = makeWay(coordinates, type, feature.getJSONObject("properties"))
                    curWays.forEach {
                        dataset.add(it)
                    }
                }
            }
        }

        return dataset
    }


    private fun wayFromLinestring(arr: JSONArray, w: Way? = null): Way {
        val way = w ?: Way()

        for (i in 0 until arr.length()) {
            val curCoord = arr.getJSONArray(i)
            val x = curCoord.getDouble(0)
            val y = curCoord.getDouble(1)
            way.addPoint(x, y)
        }
        return way
    }

    private fun makePOI(coordinates: JSONArray): POI {
        return POI(
            coordinates.getDouble(0),
            coordinates.getDouble(1),
            if (coordinates.length() == 3) coordinates.getDouble(2) else -1.0
        )
    }

    private fun makeAnnotation(coordinates: JSONArray, properties: JSONObject): Annotation {
        return Annotation(
            properties.getLong("id"), coordinates.getDouble(0), coordinates.getDouble(1),
            properties.getString("text"), properties.getString("annotationtype")
        )
    }

    private fun makeWay(coords: JSONArray, geomType: String, properties: JSONObject): ArrayList<Way> {
        //Log.d("jsontest", "makeWay: geomType=" + geomType);
        val ways = arrayListOf<Way>()

        when (geomType) {
            "LineString" -> {


                val w = wayFromLinestring(coords)
                addProperties(w, properties)
                ways.add(w)
            }

            // We cannot deal with polygons with holes just yet, so just take the first LinearRing
            "Polygon" -> {

                val polygon = wayFromLinestring(coords.getJSONArray(0))
                addProperties(polygon, properties)
                ways.add(polygon)
            }

            "MultiLineString", "MultiPolygon" -> {

                for (i in 0 until coords.length()) {
                    ways.addAll(makeWay(coords.get(i) as JSONArray, geomType.substring(5), properties))
                }
            }
        }

        return ways
    }


    private fun addProperties(f: Feature, properties: JSONObject) {

        val keys = properties.keys()

        while (keys.hasNext()) {
            val key = keys.next() as String
            f.addTag(key, properties.getString(key))
        }
    }
}


