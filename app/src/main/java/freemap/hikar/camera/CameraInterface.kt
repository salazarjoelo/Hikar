/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.camera

import android.content.Context
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import android.os.Build
import kotlin.math.atan2

class CameraInterface(private val ctx: Context, val onHfovCalculated: (Float)->Unit) {
    fun getHfov() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val cameraMgr = ctx.getSystemService(Context.CAMERA_SERVICE) as CameraManager
            cameraMgr.cameraIdList.forEach {
                val characteristics = cameraMgr.getCameraCharacteristics(it)
                if (characteristics.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_BACK) {
                    val frameSize = characteristics.get(CameraCharacteristics.SENSOR_INFO_PHYSICAL_SIZE)
                    val focalLength = characteristics.get(CameraCharacteristics.LENS_INFO_AVAILABLE_FOCAL_LENGTHS)
                    if (focalLength.isNotEmpty()) {
                        onHfovCalculated(
                            (2 * (180.0 / Math.PI) * atan2(
                                frameSize.width.toDouble(),
                                focalLength[0].toDouble() * 2
                            )).toFloat()
                        )
                    }
                }
            }
        }
    }
}